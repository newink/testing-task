package org.test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TwoSeven {

    private TwoSeven() {}

    public static List<String> run() {
        return IntStream.rangeClosed(1, 100).mapToObj(value -> {
            String out = String.valueOf(value);
            if (value % 2 == 0) {
                out = "Two";
            }
            if (value % 7 == 0) {
                out = "Seven";
            }
            if (value % 7 == 0 && value % 2 == 0) {
                out = "TwoSeven";
            }
            return out;
        }).collect(Collectors.toList());
    }
}
