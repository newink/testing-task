package org.test;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CountWords {

    public static Map<String, Long> count(String text) {
        Map<String, Long> counts = new TreeMap<>();
        String erasedPunctuation = text.replaceAll("[!?,.]", "");
        Arrays.stream(erasedPunctuation.split("\\s+")).forEach(s -> {
            if (!s.isEmpty()) {
                s = s.toLowerCase();
                if (counts.containsKey(s)) {
                    counts.put(s, counts.get(s) + 1);
                } else {
                    counts.put(s, 1L);
                }
            }
        });
        LinkedHashMap<String, Long> returnedMap = new LinkedHashMap<>();
        counts.entrySet().
                stream().sorted(Comparator.comparingLong(value -> ((Map.Entry<String, Long>) value).getValue()).reversed())
                .collect(Collectors.toList()).forEach(stringLongEntry -> returnedMap.put(stringLongEntry.getKey(), stringLongEntry.getValue()));
        return returnedMap;
    }
}
