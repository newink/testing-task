package org.test;

import java.math.BigInteger;

public class Function {


    public static BigInteger run(BigInteger m, BigInteger r) {
        if (m == null || r == null) {
            throw new IllegalArgumentException("Входные данные не могут быть пустыми");
        }
        if (r.compareTo(m) == 1) {
            throw new IllegalArgumentException("r не может быть больше m!");
        }
        BigInteger subtraction = m.subtract(r);
        return factorial(m).divide(factorial(r).multiply(factorial(subtraction)));
    }

    private static BigInteger factorial(BigInteger n) {
        BigInteger fact = BigInteger.ONE;
        if (!((n.equals(BigInteger.ZERO)) || (n.equals(BigInteger.ONE)))) {
            fact = n;
            while (n.compareTo(BigInteger.ONE) == 1) {
                n = n.subtract(BigInteger.ONE);
                fact = fact.multiply(n);
            }
        }
        return fact;
    }
}
