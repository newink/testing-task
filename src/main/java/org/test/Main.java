package org.test;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("---- Задание 1 ----");
        System.out.println(TwoSeven.run());

        System.out.println("---- Задание 2 ----");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число m: ");
        BigInteger m = scanner.nextBigInteger();
        System.out.println("Введите число r: ");
        BigInteger r = scanner.nextBigInteger();
        System.out.println("Результат: " + Function.run(m, r));

        System.out.println("---- Задание 3 ----");
        System.out.println("Введите текст: ");
        System.out.println(CountWords.count(getText(scanner)));
    }

    private static String getText(Scanner scanner) {
        StringBuilder text = new StringBuilder();
        while (scanner.hasNextLine()) {
            String str = scanner.nextLine();
            if (str.equals("exit")) {
                break;
            }
            text.append(" ").append(str);
        }
        return text.toString();
    }
}
